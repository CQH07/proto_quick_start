package com.apexsoft.xt.service;

public interface ExcelService {
    String excelToData(String excelUrl);
}
