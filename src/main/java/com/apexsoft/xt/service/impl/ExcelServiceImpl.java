package com.apexsoft.xt.service.impl;

import com.apexsoft.xt.service.ExcelService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;


import static org.apache.poi.ss.usermodel.CellType.STRING;

@Service
public class ExcelServiceImpl implements ExcelService {

    Logger log = LoggerFactory.getLogger(ExcelServiceImpl.class);
    /**
     * proto等属性值根据接口文档进行转换
     * @param excelUrl 文档地址
     * @return
     */
    public String excelToData(String excelUrl){
        List<String> list = new ArrayList<>();
        try(FileInputStream fis=new FileInputStream(excelUrl);){
            Workbook workbook= null;
            workbook = new HSSFWorkbook(fis);
            Integer sheetNum = workbook.getNumberOfSheets();
            // 遍历sheet
            for (int i = 0; i < sheetNum ; i ++){
                Sheet sheet = workbook.getSheetAt(i);
                // 获取所有行
                Integer rowNum = sheet.getLastRowNum();
                // proto属性解析结果
                StringBuilder sb = new StringBuilder("\r\n");
                // esbService请求体解析结果
                StringBuilder requestSb = new StringBuilder("\r\n");
                // 微服务请求体解析结果
                StringBuilder objSb = new StringBuilder("\r\n");
                log.info("=======================================");
                if(rowNum <= 0) break;
                for (int j = 1;j < rowNum + 1 ; j ++){
                    Row row = sheet.getRow(j);
                    // 记录数
                    String recordNum = getCellValue(row,0);
                    // 属性名
                    String property = getCellValue(row,1);
                    // 备注
                    String remark = getCellValue(row,2);
                    // 是否必传
                    String isNeed = getCellValue(row,3);
                    // 属性类型
                    String propertyType = getProtoType(getCellValue(row,4));
                    // 默认值
                    String defaultVal = getCellValue(row,5);
                    // 获取转换后默认值
                    String defaultVal1 = getDefaultVal(propertyType,defaultVal);
                    // 使用jsonUtils做JSONObject的解析
                    String jsonUtilType = getJsonUtilType(propertyType);
                    String jsonDefault = StringUtils.equals("int32",propertyType) ? "0" : "\"\"";
                    sb.append(propertyType + " " + property+ " = " + recordNum+";  //"+remark+"\r\n");
                    requestSb.append("request.put(\""+property+"\","+ jsonUtilType +"(obj , \""+property+"\"," +jsonDefault+"));  // "+remark+"\r\n");
                    String getPropertyName = "request.get"+property.substring(0, 1).toUpperCase() + property.substring(1)+"()";
                    // 该字段为必填，并且默认值不为空时，jsonObject塞值时加上默认值
                    if(StringUtils.equals("n",isNeed.toLowerCase())&&!StringUtils.isEmpty(defaultVal)){
                        objSb.append("obj.put(\""+property+"\",Optional.ofNullable("+getPropertyName+").orElse("+defaultVal1+"));"+"\r\n");
                    }else {
                        objSb.append("obj.put(\""+property+"\","+getPropertyName+");"+"\r\n");
                    }
                }
                log.info(sb.toString());
                log.info(requestSb.toString());
                log.info(objSb.toString());
                log.info("=======================================");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }


    /**
     * 获取 java默认值
     * @param type
     * @param defaultVal
     * @return
     */
    private static String getDefaultVal(String type,String defaultVal){
        switch (type){
            case "int32":
            case "bool":
            case "double":
                return defaultVal;
            case "float":
                return defaultVal+"f";
            case "int64":
                return defaultVal+"l";
            default:
                return "\""+defaultVal+"\"";
        }
    }

    /**
     * 根据文件文件的属性类型，生成对应的proto类型,如有其他类型可自行补充
     * @param type
     * @return
     */
    private static String getProtoType(String type){
        if (StringUtils.isEmpty(type)) return "string";
        String lowerType = type.toLowerCase();
        if (lowerType.contains("boolean") || lowerType.equals("bool")){
            return "bool";
        }else if(lowerType.contains("double")){
            return "double";
        }else if(lowerType.contains("float")){
            return "float";
        }else if(lowerType.contains("long")){
            return "int64";
        }else if(lowerType.contains("bytestring")||lowerType.contains("bytes")||lowerType.contains("byte")){
            return "bytes";
        }else if(lowerType.contains("int")||lowerType.contains("integer")){
            return "int32";
        }else if(lowerType.contains("string")||lowerType.contains("varchar")){
            return "string";
        }else {
            return "string";
        }
    }

    /**
     *  获取excel 单元格中的值
     * @param row
     * @param i
     * @return
     */
    private static String getCellValue(Row row, Integer i){
        if(row.getCell(i) == null) return "";
        row.getCell(i).setCellType(STRING);
        return row.getCell(i).getStringCellValue();
    }

    /**
     * JsonUtil 不同类型的返回结果
     * @param propertyType
     * @return
     */
    private static String getJsonUtilType(String propertyType){
        switch (propertyType){
            case "int32":
                return "JsonUtil.optInt";
            case "int64":
                return "JsonUtil.optLong";
            case "bool":
                return "JsonUtil.optBoolean";
            case "float":
                return "JsonUtil.optFloat";
            case "double":
                return "JsonUtil.optDouble";
            default:
                return "JsonUtil.optString";
        }
    }
}
