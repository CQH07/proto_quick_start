package com.apexsoft.xt.controller;

import com.apexsoft.xt.service.ExcelService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("excel")
@RequestMapping("excel")
@RestController
public class ExcelController {
    @Autowired
    ExcelService excelService;

    @GetMapping("excel")
    public String changeData(@RequestParam("url")String url){
        return excelService.excelToData(url);
    }

}

